from flask import request
from flask_restful import Resource
from src.models import db, Hello

class HelloResource(Resource):

    def __init__(self):
        self.many_schema = Hello.Schema(many=True)
        self.single_schema = Hello.Schema()

    def get(self):
        hellos = Hello.query.all()
        return {'status': 'success', 'data': self.many_schema.dump(hellos).data}, 200

    def post(self):
        json_data = request.get_json(force=True)
        if not json_data:
               return {'message': 'No input data provided'}, 400
        # Validate and deserialize input
        data, errors = self.single_schema.load(json_data)
        if errors:
            return errors, 422
        hello = Hello.query.filter_by(name=data['name']).first()
        if hello:
            return {'message': 'Hello already exists'}, 400
        hello = Hello(
            name=json_data['name']
            )

        db.session.add(hello)
        db.session.commit()

        result = self.single_schema.dump(hello).data

        return { "status": 'success', 'data': result }, 201
