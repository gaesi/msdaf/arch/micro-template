import os

# You need to replace the next values with the appropriate values for your configuration

basedir = os.path.abspath(os.path.dirname(__file__))
SQLALCHEMY_ECHO = False
SQLALCHEMY_TRACK_MODIFICATIONS = True

#### Set database accondingly to your desired option ###

# Postgres URI
# SQLALCHEMY_DATABASE_URI = "postgresql://username:password@localhost/database_name"

# Sqlite URI
SQLALCHEMY_DATABASE_URI = "sqlite:///app.db"
