from flask import Blueprint
from flask_restful import Api
import pyclbr

from src.registry import register, Registry
from src.resources import *

api_bp = Blueprint('/api', __name__)
api = Api(api_bp)

registry_succeeded = register()
if registry_succeeded:
    print(' * Registering manage routes...')
    api.add_resource(Registry, '/manage/<param>')

# Add your resources here, for example:
# api.add_resource(Hello, '/world')
api.add_resource(HelloResource, '/hellos')
